 <?php

 	// Скрипт для импорта данных в базу из csv файла. 
 	// Необходимо передать параметр  - путь до csv файла


 	function dbConnect(){

 		$databaseHost = 'localhost';
		$databaseName = 'ref_book';
		$databaseUsername = 'root';
		$databasePassword = 'newpass';

		$mysqli = mysqli_connect($databaseHost, $databaseUsername, $databasePassword, $databaseName); 

		return $mysqli;

 	}

 	// Функция для фильтрации данных
	function filter($link, $val, $access_tags = false){

	    return $access_tags ? strip_tags(mysqli_real_escape_string($link, $val), "<b><u><br>") : htmlspecialchars(mysqli_real_escape_string($link, $val));

	}




	function main($argv, $argc){

		$link = dbConnect();

		$row = 1;

	 	if($argc < 2){
	 		echo "Необходимо передать путь до csv файла!\n";
	 		return;
	 	}


		$handle = @fopen($argv[1], "r") or die("Не удалось открыть файл!\n");

		while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
		    
		    $num = count($data);
		   
		    if($row == 1){
		    	echo "\nКоличество полей в строке: " . $num . "\n";
		    	echo "Пропускаем первую строку: \n";
		    	$row++;
		    	continue;
		    }

        	echo "На очереди запись с id: " . implode(" - ", $data) . "\n";

        	if(abs(intval($data[3])) < 1 || abs(intval($data[3])) > 5){
        		echo "Передано некорректное значение сложности, исправляем на 3\n";
        		$data[3] = 3;
        	}

        	$data[1] = filter($link, $data[1]);

        	// Разрешаем некоторые теги
        	$data[2] = filter($link, $data[2], true);
			
			$data[3] = abs(intval($data[3]));

    	   	mysqli_query($link, "INSERT INTO entry(title, description, complexity) VALUES('$data[1]', '$data[2]', $data[3])")
			or die("Не удалось выполнить запрос. Попробуйте позже. \n" . mysqli_error($link));

		    echo "\n";	

		}

		 // Получение всех записей из таблицы entry
		$result = mysqli_query($link, "SELECT * FROM entry")  or die("Что-то пошло не так. Попробуйте чуть позже. \n");

		// Выводим все значения
		while($res = mysqli_fetch_array($result)){
			echo implode(" ", $res) . "\n";
		}

		fclose($handle);

		echo "\n";
	}

	main($argv, $argc);

?> 