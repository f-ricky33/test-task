<?php if(session_id() == '') {
    session_start();
} ?>
<?php

include_once($_SERVER['DOCUMENT_ROOT'] . "/connection.php");
include_once($_SERVER['DOCUMENT_ROOT'] . "/functions.php");

if(isset($_POST['login']) && isset($_POST['password'])) {

    //Получаем данные, функция защиты от
	$user = filter($mysqli, $_POST['login']);
	$pass = mysqli_real_escape_string($mysqli, $_POST['password']);

	if($user == "" || $pass == "") {
        echo json_encode(array('success' => false, 'message' => 'Поля логин и пароль должны быть заполнены.'));
    } else {
		$result = mysqli_query($mysqli, "SELECT * FROM users WHERE login='$user' AND password=md5('$pass')")
					or die(json_encode(array('success' => false, 'message' => 'Не удалось выполнить запрос.')));

		$row = mysqli_fetch_assoc($result);
		
		if(is_array($row) && !empty($row)) {
			$validuser = $row['login'];
			$_SESSION['valid'] = $validuser; // Сессионный ключ надо генерировать хитрее, для примера сойдет
			$_SESSION['name'] = $row['name'];
			$_SESSION['id'] = $row['id'];
            echo json_encode(array('success' => true, 'message' => 'Успешно авторизовались'));
		} else {
            echo json_encode(array('success' => false, 'message' => 'Неправильный логин или пароль'));
        }
	}
}