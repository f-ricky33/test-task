<div id="signin" class="modal">
    <form id="loginFormButton" class="modal-content animate" method="post">
        <div class="imgcontainer">
            <span onclick="document.getElementById('signin').style.display='none'" class="close" title="Close Modal">×</span>
        </div>

        <div class="container">
            <label for="login"><b>Логин</b></label>
            <input id="login" type="text" placeholder="Введите логин" name="login" required>

            <label for="password"><b>Пароль</b></label>
            <input id="password" type="password" placeholder="Введите пароль" name="password" required>

            <button type="submit" name="submit">Войти</button>
            <span id="server_login_message" style="margin: 10px; text-align: center"></span>
        </div>
    </form>
</div>