<div id="signup" class="modal">
    <form id="registerFormButton" class="modal-content animate" method="post">
        <div class="imgcontainer">
            <span onclick="document.getElementById('signup').style.display='none'" class="close" title="Close Modal">×</span>
        </div>

        <div class="container">
            <label for="register_name"><b>Имя</b></label>
            <input id="register_name" type="text" placeholder="Введите имя" name="name" required>

            <label for="register_surname"><b>Фамилия</b></label>
            <input id="register_surname" type="text" placeholder="Введите фамилию" name="surname" required>

            <label for="register_email"><b>Email</b></label>
            <input id="register_email" type="email" placeholder="Введите фамилию" name="email" required>

            <label for="register_login"><b>Логин</b></label>
            <input id="register_login" type="text" placeholder="Введите логин" name="login" required>

            <label for="input_register_password"><b>Пароль</b></label>
            <input id="input_register_password" type="password" placeholder="Введите пароль" name="password" required>

            <label for="confirm_password"><b>Повторите пароль</b></label>
            <input id="confirm_password" type="password" placeholder="Введите пароль еще раз" required>

            <button type="submit" name="submit">Зарегистрироваться</button>
            <span id="server_register_message" style="margin: 10px; text-align: center"></span>
        </div>
    </form>
</div>