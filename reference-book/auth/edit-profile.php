<?php if(session_id() == '') {
    session_start();
} ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Страница редактирования профиля</title>
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="/favicon.ico">
</head>

<body>


<?php
    // Добавляем к коду файл для коннекции к бд
    include_once($_SERVER['DOCUMENT_ROOT'] . "/connection.php");
    include_once($_SERVER['DOCUMENT_ROOT'] . "/functions.php");

    if(!isset($_SESSION['valid'])) {
        redirect_to('/');
    }
?>

<?php include_once($_SERVER['DOCUMENT_ROOT'] . "/header.php");	?>

<?php

    define("MIN_PASSWORD_LEN", 6);

    if (isset($_POST['update'])) {

        $id = absint($_POST['id']);

        // Получаем запись по id
        $res = get_data_by_id($mysqli, $id, 'users');

        if(!$id){
            echo show_message('По данному id нет пользователя.', 'red');
            return;
        }

        $name = $res['name'];
        $surname = $res['surname'];
        $login = $res['login'];
        $email = $res['email'];
        $password = $res['password'];

        // Экранирование данных для защиты от sqli и xss
        $name = !empty($_POST['name']) ? filter($mysqli, $_POST['name']) : $name;
        $surname = !empty($_POST['surname']) ? filter($mysqli, $_POST['surname']) : $surname;
        $email =!empty($_POST['email']) ? filter($mysqli, $_POST['email']) : $email;
        $login = !empty($_POST['login']) ? filter($mysqli, $_POST['login']) : $login;
        $password = !empty($_POST['password']) ?  md5($mysqli, $_POST['password']) : $password;

        // Проверки на стороне сервера
        if ($login == "" || $name == "" || $password == "" || $surname == "" || $email == "") {
            echo show_message("Что-то пошло не так.", 'red');
        } else {

            // Проверка длины пароля
            if (strlen($password) < MIN_PASSWORD_LEN) {
                echo show_message('Минимальная длина пароля - 6 символов.', 'red');
                return;
            }

            // Проверка email
            if (!stristr($email, '@') || !stristr($email, '.')) {
                echo show_message('Неверный формат email.', 'red');
                return;
            }

            //Проверяем наличие логина или пароля в базе
            $result = mysqli_query($mysqli, "SELECT * FROM users WHERE (login='$login' OR email='$email') AND id != $id") or die (show_message('Не удалось выполнить запрос.', 'red', true));

            if(mysqli_fetch_array($result)){
                echo show_message('Пользователь с такими данными уже существует.', 'red', true);
                return;
            }

            mysqli_query($mysqli, "UPDATE users SET name='$name', surname='$surname', email='$email', login='$login', password='$password' WHERE id = $id")
            or die(show_message('Не удалось отредактировать данные. Попробуйте позже.', 'red', true));

            echo show_message('Успешно отредактировали данные.', 'green');

        }
    }

?>

<?php

    // Обработка GET-запроса

    // Получаем id из url, приводим к типу int
    $id = abs($_GET['id']);

    // Получаем запись по id
    $res = get_data_by_id($mysqli, $id, 'users');

    if(!$res){
        return;
    }

    $name = $res['name'];
    $surname = $res['surname'];
    $login = $res['login'];
    $email = $res['email'];

?>

<a class="link-button ref-btn blue-violet-btn" href="/auth/users.php">Все пользователи</a> <a class="link-button ref-btn forestgreen-btn"  href="/">Вернуться на главную</a>

<form style="margin: 20px auto" action="edit-profile.php?id=<?=$id?>" id="editFormButton" class="modal-content animate" method="post">
    <div class="container">

        <input type="hidden" name="id" value="<?=$id?>" >

        <label for="name"><b>Имя</b></label>
        <input type="text" placeholder="Введите новое имя" name="name" value="<?= $name ?>">

        <label for="surname"><b>Фамилия</b></label>
        <input type="text" placeholder="Введите фамилию" name="surname" value="<?= $surname ?>">

        <label for="login"><b>Логин</b></label>
        <input type="text" placeholder="Введите новый логин" name="login" value="<?= $login ?>">

        <label for="email"><b>Email</b></label>
        <input type="email" placeholder="Введите новый логин" name="email" value="<?= $email ?>">

        <label for="password"><b>Пароль</b></label>
        <input type="password" placeholder="Введите новый пароль" name="password">

        <button type="submit" name="update">Редактировать</button>

    </div>
</form>

<?php include_once($_SERVER['DOCUMENT_ROOT'] . "/footer.php");	?>

</body>
</html>
