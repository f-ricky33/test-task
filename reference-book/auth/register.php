<?php

include_once($_SERVER['DOCUMENT_ROOT'] . "/connection.php");
include_once($_SERVER['DOCUMENT_ROOT'] . "/functions.php");

define("MIN_PASSWORD_LEN", 6);

if(isset($_POST['login']) && isset($_POST['password'])) {

	// Экранирование данных для защиты от sqli и xss
	$name =filter($mysqli, $_POST['name']);
	$surname = filter($mysqli, $_POST['surname']);
	$email = filter($mysqli, $_POST['email']);
	$login = filter($mysqli, $_POST['login']);
	// Пароль нигде выводить не нужно, не защищаемся от xss
	$password = mysqli_real_escape_string($mysqli, $_POST['password']);

	// Проверки на стороне сервера
	if($login == "" || $name == "" || $password == "" || $surname == "" || $email == "") {
		echo json_encode(array('success' => false, 'message' => 'Заполнены не все поля.'));
	} else {

		// Проверка длины пароля
		if(strlen($password) < MIN_PASSWORD_LEN){
			echo json_encode(array('success' => false, 'message' => 'Минимальная длина пароля - 6 символов.'));
			return;
		}

		// Проверка email
		if(!stristr($email, '@') || !stristr($email, '.')){
			echo json_encode(array('success' => false, 'message' => 'Неверный формат email.'));
			return;
		}

		//Проверяем наличие логина или пароля в базе
		$result = mysqli_query($mysqli, "SELECT * FROM users WHERE login='$login' OR email='$email'") or die ('Не удалось выполнить запрос.');

		if(mysqli_fetch_array($result)){
			echo json_encode(array('success' => false, 'message' => 'Пользователь с такими данными уже существует.'));
			return;
		}

		mysqli_query($mysqli, "INSERT INTO users(name, surname, login, email, password) VALUES('$name', '$surname', '$login','$email', md5('$password'))")
		or die(json_encode(array('success' => false, 'message' => 'Не удалось создать пользователя. Попробуйте позже.')));

		echo json_encode(array('success' => true, 'message' => 'Успешно зарегистрировались. Теперь вы можете войти под своей учетной записью.'));

	}
}
?>

