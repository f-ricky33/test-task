<?php if(session_id() == '') {
    session_start();
} ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Все пользователи</title>
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="/favicon.ico">
</head>

<body>

<?php include_once($_SERVER['DOCUMENT_ROOT'] . "/header.php");	?>

<?php include($_SERVER['DOCUMENT_ROOT'] . "/auth/view-users.php");	?>

<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php");	?>

<script src="/assets/js/jquery-3.5.1.min.js"></script>
<script src="/assets/js/main.js"></script>
</body>
</html>
