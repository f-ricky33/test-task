<?php if(session_id() == '') {
    session_start();
} ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Страница профиля</title>
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="/favicon.ico">
</head>

<body>
<?php include_once($_SERVER['DOCUMENT_ROOT'] . "/header.php");	?>

<?php

    // Добавляем к коду файл для коннекции к бд
    include_once($_SERVER['DOCUMENT_ROOT'] . "/connection.php");

    $id = abs($_GET['id']);

    if($id <= 0){
        die('Мы не смогли найти ваш профиль! <br><a class="link-button" style="background-color: #f44336; width:auto;" href="/">Вернуться на главную</a>');
    }

    // Получение всех полей из таблицы users
    $result = mysqli_query($mysqli, "SELECT * FROM users where id = $id");
?>

<br/>
<table>
    <tr style="background-color: #CCCCCC">
        <th>id</th>
        <th>Имя</th>
        <th>Фамилия</th>
        <th>Логин</th>
        <th>Email</th>
    </tr>
    <?php
    while($res = mysqli_fetch_array($result)): ?>
        <tr id="user_<?= $res['id'] ?>">
            <td><?=$res['id']?></td>
            <td><?=$res['name']?></td>
            <td><?=$res['surname']?></td>
            <td><?=$res['login']?></td>
            <td><?=$res['email']?></td>
        </tr>
    <?php endwhile; ?>
</table>

<?php if(isset($_SESSION['valid'])):?> <a  class="link-button" href="/auth/edit-profile.php?id=<?=$id ?>" style="width:auto; background-color: #336699">Редактировать профиль</a><?php endif; ?> <a class="link-button" style="background-color: blueviolet; width:auto;" href="/auth/users.php">Все пользователи</a> <a class="link-button" style="background-color: #f44336; width:auto;" href="/">Вернуться на главную</a>

