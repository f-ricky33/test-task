<?php if(session_id() == '') {
    session_start();
} ?>
<?php

// Добавляем к коду файл для коннекции к бд
include_once($_SERVER['DOCUMENT_ROOT'] . "/connection.php");

// Получение всех записей из таблицы entry
$result = mysqli_query($mysqli, "SELECT * FROM users order by id");
?>
<a class="link-button" style="background-color: blueviolet; width:auto;" href="/">Вернуться на главную</a>
<br/>
<table>
    <tr style="background-color: #CCCCCC">
        <th>id</th>
        <th>Логин</th>
        <th>Имя</th>
        <th>Фамилия</th>
        <th>Email</th>
    </tr>
    <?php
    while($res = mysqli_fetch_array($result)): ?>
        <tr id="entry_<?= $res['id'] ?>">
            <td><?=$res['id']?></td>
            <td><a href="/auth/profile.php?id=<?=$res['id']?>"?><?=$res['login']?></a></td>
            <td><?=$res['name']?></td>
            <td><?=$res['surname']?></td>
            <td><?=$res['email']?></td>
        </tr>
    <?php endwhile; ?>
</table>