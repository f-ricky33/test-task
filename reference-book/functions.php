<?php

// Функция для фильтрации данных
function filter($link, $val, $access_tags = false){
    return $access_tags ? strip_tags(mysqli_real_escape_string($link, $val), "<b><u><br>") : strip_tags(mysqli_real_escape_string($link, $val));
}

// Функция возвращает данные из таблицы from по id
function get_data_by_id($link, $id, $from){

    $result = mysqli_query($link, "SELECT * FROM $from WHERE id=$id") or die(show_message("Что-то пошло не так.", 'red'));

    $row = mysqli_fetch_array($result);

    return $row;
}

// Функция для вывода сообщений на страницу
function show_message($mes, $color, $show_button = false){

    $message = "<p style=\"color: $color\">$mes</p><br/>";

    if($show_button)
        $message .= "<a class=\"link-button\" style=\"background-color: forestgreen; width:auto;\" href='javascript: history.back(1)'>Вернуться назад</a>";

    return $message;
}

// Функция для перенаправления и завершения скрипта
function redirect_to($url){
    exit("<meta http-equiv='refresh' content='0; url=$url'>");
}

// Получение абсолютного целого значения
function absint($val){
    return abs(intval($val));
}