<?php if(session_id() == '') {
    session_start();
} ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Ошибка 404</title>
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="/favicon.ico">
</head>

<body>
<?php include_once($_SERVER['DOCUMENT_ROOT'] . "/header.php");	?>
<?php  if(isset($_SESSION['valid'])):?>

    <span style="font-size: 16px">Привет, <?php echo $_SESSION['name'] ?></span>
    <a class="link-button" href="entry/add.php" style="width:auto; background-color: blueviolet">Добавить запись</a> <a class="link-button" href="/auth/profile.php?id=<?=$_SESSION['id']?>" style="width:auto; background-color: #336699">Профиль</a> <a class="link-button" style="background-color: #f44336; width:auto;" href="/auth/logout.php" onClick="return confirm('Вы уверены, что хотите выйти?')">Выйти</a>

<?php else: ?>

    <button onclick="document.getElementById('signin').style.display='block'" style="width:auto;">Вход</button>
    <button onclick="document.getElementById('signup').style.display='block'" style="width:auto;">Регистрация</button>
    <?php include("auth/login-form.php");?>
    <?php include("auth/register-form.php");?>

<?php endif; ?>

<p style="text-align: center; font-size: 24px;">К сожалению такой страницы нет</p>

<?php include("footer.php");	?>


<script src="assets/js/jquery-3.5.1.min.js"></script>
<script src="assets/js/main.js"></script>
</body>
</html>
