// Константа для хранения минимальной длины пароля
const MIN_PASSWORD_LEN = 6;

/* Функция для отправки ajax запроса на сервер
- Принимает в качестве параметров обработчик запроса, метод запроса,
- объект this, id элемента вывода и uri для редиректа
 */
function sendAjax(target, method, this_request, message_id, redirect_uri){
    $.ajax({
        type: method,
        url: target,
        data: $(this_request).serialize(),
        success: (response) => {

            console.log(response);
            let jsonData = $.parseJSON(response);

            if(jsonData.success) {

                $(message_id).html(jsonData.message).css('color', 'green');

                setTimeout(() => {
                    window.location.replace(redirect_uri);
                }, 1500);
            }
            else {
                $(message_id).html(`${jsonData.message}`).css('color', 'red');
            }
        },
        error: (req, text, error) => {
            $(message_id).html('Что-то пошло не так, попробуйте чуть позже.').css('color', 'red');
        },
    });
}

// AJAX запрос для авторизации
$(document).ready(() => {
    $('#loginFormButton').submit(function(e) {

        $('#server_login_message').html('');
        e.preventDefault();

        sendAjax('auth/login.php', 'POST', this, '#server_login_message', '/');

    });
});

// AJAX запрос для регистрации
$(document).ready(() => {
    $('#registerFormButton').submit(function(e) {

        $('#server_register_message').html('');
        e.preventDefault();

        // Если введенные пароли не совпадают
        if ($('#input_register_password').val() == $('#confirm_password').val()) {

            // Проверка длины пароля на стороне фронтенда
            if($('#input_register_password').val().length < MIN_PASSWORD_LEN){
                $('#server_register_message').html('Минимальная длина пароля - 6 символов.').css('color', 'red');
                return;
            }

            sendAjax('auth/register.php', 'POST', this, '#server_register_message', '/');
            //$('#server_register_message').html('Успешно зарегистрировались.').css('color', 'green');
        } else {
            $('#server_register_message').html('Введенные пароли не совпадают.').css('color', 'red');
        }
    });
});

// Скрыть модальное окно при клике в любом месте
$(window).click(function(event) {
    if (event.target === document.getElementById('signin')) {
        $('#signin').css('display', 'none');
    }

    if (event.target === document.getElementById('signup')) {
        $('#signup').css('display', 'none');
    }
});


// Функция обработки сортировки
$(() => {
    $("#id_sorting").change(() => {
        let value = $("#id_sorting option:selected" ).val();

        // Get запрос для сортировки
        $.get(`entry/sorting.php?sorting=${value}`, (data, status) => {

            let new_data = $.parseJSON(data);
            $("table").find("tr:gt(0)").remove();

            new_data['result'].forEach((item, i, arr) => {

                row = `<tr id="entry_${item['id']}"> <td>${item['id']}</td> <td>${item['title']}</td> <td>${item['description']}</td> <td>${item['complexity']}</td> `;

                if(new_data['can_edit']) {
                    row += `<td> <a id="edit_button" class="link-button" href="entry/edit.php?id=${item['id']}" style="width:auto;">Редактировать</a> <a class="link-button" style="background-color: #f44336; width:auto;" href="entry/delete.php?id=${item['id']}" onClick="return confirm('Вы действительно хотите удалить?')">Удалить</a></td>`;
                }

                row += `</tr>`;
                $('table tr:last').after(row);
            });

        });
    });
});