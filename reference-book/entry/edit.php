<?php if(session_id() == '') {
    session_start();
} ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Страница редактирования материала</title>
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="/favicon.ico">
</head>

<body>


<?php
    // Добавляем к коду файл для коннекции к бд
    include_once($_SERVER['DOCUMENT_ROOT'] . "/connection.php");
    include_once($_SERVER['DOCUMENT_ROOT'] . "/functions.php");

    if(!isset($_SESSION['valid'])) {
        redirect_to('/');
    }
?>

<?php include_once($_SERVER['DOCUMENT_ROOT'] . "/header.php");	?>

<?php

    // Обработка post запроса
    if(isset($_POST['update']))
    {
        $id = absint($_POST['id']);
        $title = filter($mysqli, $_POST['title']);
        $desc =  filter($mysqli, $_POST['description'], true);
        $cpl = absint($_POST['complexity']);

        // Проверяем пустые поля
        if(empty($title) || empty($desc)) {

            if(empty($title)) {
                echo show_message('Поле заголовок необходимо заполнить', 'red');
            }

            if(empty($desc)) {
                echo show_message('Поле описание необходимо заполнить', 'red');
            }

            if($id < 1) {
                echo show_message('Что вы пытаетесь отредактировать?', 'red');
            }

        } else {

            // Значение сложности по умолчанию
            if($cpl < 1 || $cpl > 5) {
                $cpl = 3;
            }

            $result = mysqli_query($mysqli, "UPDATE entry SET title='$title', description='$desc', complexity='$cpl' WHERE id = $id")
                or die('<p style="color: red">Что-то пошло не так. Попробуйте чуть позже</p><br/>');

            if(!$result){
                echo show_message('Не удалось отредактировать', 'red');
                return;
            }

             echo show_message('Успешно отредактировали', 'green');

        }
    }
?>

<?php

    // Обработка GET-запроса

    // Получаем id из url, приводим к типу int
    $id = absint($_GET['id']);

    // Получаем запись по id
    $result = mysqli_query($mysqli, "SELECT * FROM entry WHERE id=$id")  or die(show_message('Что-то пошло не так. Попробуйте чуть позже', 'red', true));

    if($res = mysqli_fetch_array($result)) {
        $title = $res['title'];
        $desc = $res['description'];
        $cpl = $res['complexity'];

    } else {
        echo show_message('Нет записи по данному id', 'red');
        return;
    }
?>

<a id="edit_button" class="link-button ref-btn blue-violet-btn" href="/">Вернуться на главную</a>

<form style="margin: 20px auto" action="edit.php?id=<?=$id?>" id="editFormButton" class="modal-content animate" method="post">
    <div class="container">

        <input type="hidden" name="id" value="<?=$id?>" >

        <label for="title"><b>Название</b></label>
        <input id="title" type="text" placeholder="Введите новый заголовок" name="title" value="<?= $title ?>" required>

        <label for="description"><b>Описание</b></label>
        <textarea id="description" placeholder="Введите новое описание" name="description" required><?= $desc ?></textarea>

        <label for="complexity"><b>Сложность</b></label>
        <input id="complexity" type="number" placeholder="Введите сложность" name="complexity" value="<?= $cpl ?>">

        <button type="submit" name="update">Редактировать</button>

    </div>
</form>

<?php include_once($_SERVER['DOCUMENT_ROOT'] . "/footer.php");	?>

</body>
</html>
