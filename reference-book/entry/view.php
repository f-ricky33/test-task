<?php if(session_id() == '') {
    session_start();
} ?>

<?php
// Добавляем к коду файл для коннекции к бд
include_once($_SERVER['DOCUMENT_ROOT'] . "/connection.php");

// Получение всех записей из таблицы entry
$result = mysqli_query($mysqli, "SELECT * FROM entry order by title desc")  or die(show_message('Что-то пошло не так. Попробуйте чуть позже', 'red', true));
?>

<select id="id_sorting" style="margin-left: auto; height: 40px; width: 200px; display: block;" name="sorting">
    <option value="byAlphabet">По алфавиту</option>
    <option value="byId">По идентификатору</option>
    <option value="byCpl">По сложности</option>
</select>

<br/>
<table>
    <tr style="background-color: #CCCCCC">
        <th>id</th>
        <th>Заголовок</th>
        <th>Описание</th>
        <th>Сложность</th>
        <?php if(isset($_SESSION['valid'])):?>
            <th>Управление</th>
        <?php endif; ?>
    </tr>
    <?php
    while($res = mysqli_fetch_array($result)): ?>
        <tr id="entry_<?= $res['id'] ?>">
            <td><?=$res['id']?></td>
            <td><?=$res['title']?></td>
            <td><?=$res['description']?></td>
            <td><?=$res['complexity']?></td>
            <?php if(isset($_SESSION['valid'])):?>
            <td> <a id="edit_button" class="link-button" href="entry/edit.php?id=<?=$res['id'] ?>" style="width:auto;">Редактировать</a> <a class="link-button" style="background-color: #f44336; width:auto;" href="entry/delete.php?id=<?=$res['id'] ?>" onClick="return confirm('Вы действительно хотите удалить?')">Удалить</a></td>
            <?php endif ?>
        </tr>
    <?php endwhile; ?>
</table>