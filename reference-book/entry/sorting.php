<?php

    if(session_id() == '') {
        session_start();
    }

    // Добавляем к коду файл для коннекции к бд
    include_once($_SERVER['DOCUMENT_ROOT'] . "/connection.php");
    include_once($_SERVER['DOCUMENT_ROOT'] . "/functions.php");

$sort_type  = filter($mysqli, $_GET['sorting']);

    $can_edit = isset($_SESSION['valid']) ? true : false;

    switch ($sort_type) {
        case "byId":
            $result = run_mysql_query($mysqli, 'id');
            echo json_encode(array('can_edit' =>  $can_edit, 'result' => $result));
            break;
        case "byAlphabet":
            $result = run_mysql_query($mysqli, 'title');
            echo json_encode(array('can_edit' =>  $can_edit, 'result' => $result));
            break;
        case "byCpl":
            $result = run_mysql_query($mysqli, 'complexity');
            echo json_encode(array('can_edit' =>  $can_edit, 'result' => $result));
            break;
        default:
            $result = run_mysql_query($mysqli, 'title', 'desc');
            echo json_encode(array('can_edit' =>  $can_edit, 'result' => $result));
    }


    // Функция для выполнения запроса с необходимыми параметрами
    function run_mysql_query($mysqli, $oder_field, $order_type =  'asc'){
        $result = mysqli_query($mysqli, "SELECT * FROM entry  ORDER BY " . $oder_field. " " . $order_type) or die(json_encode(array('status' => false, 'message' => 'Что-то пошло не так!')));

        $array = array();

        while($row = mysqli_fetch_assoc($result)) {
            $array[] = $row;
        }

        return $array;
    }