<?php if(session_id() == '') {
    session_start();
} ?>

<?php
    // Добавляем к коду файл для коннекции к бд
    include_once($_SERVER['DOCUMENT_ROOT'] . "/connection.php");
    include_once($_SERVER['DOCUMENT_ROOT'] . "/functions.php");

    if(!isset($_SESSION['valid'])) {
        redirect_to('/');
    }
?>

<?php

    // Получаем id из GET запроса
	$id = absint($_GET['id']);

	// Удаляем запись из таблицы
	$result = mysqli_query($mysqli, "DELETE FROM entry WHERE id=$id")  or die(show_message('Что-то пошло не так. Попробуй чуть позже', 'red', true));

	if(!$result){
        echo show_message('Не удалось удалить', 'red');
        return;
    }

    redirect_to('/');
?>


