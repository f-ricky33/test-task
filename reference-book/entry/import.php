<?php
    if (session_id() == '') {
        session_start();
    }

    if(!isset($_SESSION['valid'])) {
        redirect_to('/');
    }

    // Обработка post запроса
    if($_FILES['importfile']['name']) {

        // Добавляем к коду файл для коннекции к бд
        include_once($_SERVER['DOCUMENT_ROOT'] . "/connection.php");
        include_once($_SERVER['DOCUMENT_ROOT'] . "/functions.php");

        // Каталог, в который мы будем принимать файл:
        $uploaddir = './importfiles/';
        $uploadfile = $uploaddir.basename($_FILES['importfile']['name']);

        if (!file_exists( $uploaddir)) {
            mkdir( $uploaddir, 755, true);
        }

        // Копируем файл из каталога для временного хранения файлов:
        if (copy($_FILES['importfile']['tmp_name'], $uploadfile)) {

            $handle = @fopen($uploadfile, "r") or die("Не удалось открыть файл!\n");

            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

                if(absint($data[3]) < 1 || absint($data[3]) > 5){
                    echo "Передано некорректное значение сложности, исправляем на 3\n";
                    $data[3] = 3;
                }

                $data[1] = filter($mysqli, $data[1]);

                // Разрешаем некоторые теги
                $data[2] = filter($mysqli, $data[2], true);

                $data[3] = absint($data[3]);

                mysqli_query($mysqli, "INSERT INTO entry(title, description, complexity) VALUES('$data[1]', '$data[2]', $data[3])")
                or die("Не удалось выполнить запрос. Попробуйте позже. \n");

            }

            echo show_message('Успешно импортировали записи!', 'green');

        }
        else {
            echo "<script>alert('Ошибка! Не удалось загрузить файл на сервер')</script>";
            redirect_to('/entry/import.php');
            return;
        }

    }
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Главная страница</title>
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="/favicon.ico">
</head>

<body>
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/header.php");	?>
    <?php
    if(isset($_SESSION['valid'])):
        include_once($_SERVER['DOCUMENT_ROOT'] . "/connection.php");
        $result = mysqli_query($mysqli, "SELECT * FROM login");
        ?>

        <span style="font-size: 16px">Привет, <?php echo $_SESSION['name'] ?></span>
        <a class="link-button" href="entry/add.php" style="width:auto; background-color: blueviolet">Добавить запись</a> <a class="link-button" href="/" style="width:auto; background-color: #336699">Главная</a> <a class="link-button" style="background-color: #f44336; width:auto;" href="/auth/logout.php" onClick="return confirm('Вы уверены, что хотите выйти?')">Выйти</a>
    <?php endif; ?>

    <form method="POST" enctype="multipart/form-data">
        <input type="file" name="importfile">
        <input class="link-button" type="submit" value="Импортировать">
    </form>

    <?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php");	?>


    <script src="assets/js/jquery-3.5.1.min.js"></script>
    <script src="assets/js/main.js"></script>
</body>
</html>
