<?php
    if(session_id() == '') {
        session_start();
    }

    // Добавляем к коду файл для коннекции к бд
    include_once($_SERVER['DOCUMENT_ROOT'] . "/connection.php");
    include_once($_SERVER['DOCUMENT_ROOT'] . "/functions.php");

    if(!isset($_SESSION['valid'])) {
        redirect_to('/');
    }

    // Получаем все записи
    $result = mysqli_query($mysqli, "SELECT * FROM entry");

    // Открываем csv файл для записи
    $fcsv = fopen('export-entries.csv', 'w');

    while($row = mysqli_fetch_row($result)){
        if(!fputcsv($fcsv, $row)){
            die('Что-то пошло не так.');
        }
    }

    echo '<script>alert(\'Успешно экспортировали записи\')</script>';

    redirect_to('/');


