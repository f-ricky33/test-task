<?php if(session_id() == '') {
    session_start();
} ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Страница добавления материала</title>
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="/favicon.ico">
</head>

<body>

<?php
    // Добавляем к коду файл для коннекции к бд
    include_once($_SERVER['DOCUMENT_ROOT'] . "/connection.php");
    include_once($_SERVER['DOCUMENT_ROOT'] . "/functions.php");

    if(!isset($_SESSION['valid'])) {
        redirect_to('/');
    }
?>

<?php include_once($_SERVER['DOCUMENT_ROOT'] . "/header.php");	?>

<?php

// Обработка post запроса
    if(isset($_POST['add_entry']))
    {
        $title = filter($mysqli, $_POST['title']);
        $desc =  filter($mysqli, $_POST['description'], true);
        $cpl = absint($_POST['complexity']);

        // Проверяем пустые поля
        if(empty($title) || empty($desc)) {

            if(empty($title)) {
                echo show_message('Поле заголовок необходимо заполнить', 'red');
                return;
            }

            if(empty($desc)) {
                echo show_message('Поле описание необходимо заполнить', 'red');
                return;
            }

        } else {

            // Значение сложности по умолчанию
            if($cpl < 1 || $cpl > 5) {
                $cpl = 3;
            }

            mysqli_query($mysqli, "INSERT INTO entry(title, description, complexity) VALUES('$title', '$desc', $cpl)")
            or die(show_message('Что-то пошло не так. Попробуйте чуть позже', 'red', true));

            echo show_message('Успешно добавлен материал', 'green');

        }
    }
?>

<a id="edit_button" class="link-button ref-btn blue-violet-btn" href="/">Вернуться на главную</a>

<form style="margin: 20px auto" action="add.php" id="editFormButton" class="modal-content animate" method="post">
    <div class="container">

        <input type="hidden" name="id" >

        <label for="title"><b>Название</b></label>
        <input id="title" type="text" placeholder="Введите новый заголовок" name="title" required>

        <label for="description"><b>Описание</b></label>
        <textarea id="description" placeholder="Введите новое описание" name="description" required></textarea>

        <label for="complexity"><b>Сложность</b></label>
        <input id="complexity" type="number" placeholder="Введите сложность" name="complexity" value="">

        <button type="submit" name="add_entry">Добавить</button>
        <span id="server_edit_message" style="margin: 10px; text-align: center"></span>
    </div>
</form>

<?php include_once($_SERVER['DOCUMENT_ROOT'] . "/footer.php");	?>

</body>
</html>
